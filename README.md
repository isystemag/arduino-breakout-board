# Arduino Breakout Board #

The repository contains the Design Spark PCB projects for two breakout boards designed for use with Arduino boards. The first is for the Arduino UNO layout. The second is for the Arduino MEGA layout.
Unlike a shield, that covers the underlying Arduino board, this design brings solder pads to the outside of the board, enabling flying leads to be soldered to the desired connections. 

## Who do I talk to? ##

If you would like to provide feedback or what more info, drop me a line at stuart dot cording at isystem dot com. Thanks!